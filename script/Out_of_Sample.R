# giovanni.kraushaar@usi.ch
# emmanuel.esteban.urena.ruiz@usi.ch
# 2018-12-15

# -------------------------------------------------------
# -----          Out-of-Sampling Analysis           -----

# In this script we will try to fit the economic data of
# the period 1985-2018 (AFED) with a model that is asses-
# sed with data of the period 1970-1990 (DFED).

d    <- loadData(compatibility.mode = T) 
d[,-3] <- log( d[,-3] )

DFED <- selectDuringFED(d)
AFED <- selectAfterFED(d)

model <- VAR( DFED, 4 )
fo <- forecast.error( AFED, table.coefficients(model), 8 )
fo$plots
# The plots suggest that the model does not perform very 
# well with data out of sampling.
# This means that the model must be updated (re-assessed)
# with the new data as soon as it is available. I suppose 
# that happens because the time series are not stationary 
# even in trend.  

# Let's try now to reduce the time horizon. Let's see how
# a model built on data up to may 2015 can predict data 
# for the following 3 years. 

n <- nrow(AFED)
model <- VAR( AFED[-((n-11):n), ], 4 )
forecast.error( tail(AFED, 24), table.coefficients(model), 2 )

e <- forecast.error( AFED, table.coefficients(model), 3 )
e$plots

e1 <- e$error %>% head(-12)
e2 <- e$error %>% tail(12)

# H0: different mean (Y≠0);  H1: means are the same (Y=0)

Y  <- apply(e1,2,mean) - apply(e2,2,mean)
v1 <- diag(var(e1)) * (length(e1)-1)
v2 <- diag(var(e2)) * (length(e2)-1)
t  <- sqrt((v1 + v2) / ( length(e1) + length(e2) - 2 )) * qt(.025,( length(e1) + length(e2) - 2 ))

Y < t | Y > abs(t)

# For no variable we can exclude the possibility that 
# the average error is bigger.