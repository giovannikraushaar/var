# emmanuel.esteban.urena.ruiz@usi.ch
# giovanni.kraushaar@usi.ch
# 2018-11-19
# 2018-11-20 - Started working on the summary statistics - EU
# 2018-11-26 - Moved the forecast error decomp part to a new script - EU
# 2018-11-28 - Fixed BFED, DFED and AFED - GK

# In general let's produce tables here and 

# Tables ====================================================================
# This script produces tables from summary statistics and statistical tests.
#
# --------------------------------------------------------------------------
df <-loadData(compatibility.mode = TRUE)
BFED <- selectBeforeFED(df)
DFED <- selectDuringFED(df)
AFED <- selectAfterFED(df)

summary(df)
# Isn't more interesting to summary each subset?
# On average, the money supply in the US has been 1029.29 billions

sd(BFED$M)
sd(DFED$M)

plot(df$M)
# The plot shows an increasing trend of money supply since 1964, 
# with a stabilization period in the late 1990's 
# followed by a exponential increase in the late 2000's

plot(BFED(df)$M) # money supply before FED intervention
plot(DFED(df)$M) # money supply during FED intervention
plot(AFED(df)$M) # amoney supply fter FED intervention

# The graphs show the data trend before, during, and after the FED monetary reform 
# which consisted in the abandonment of federal funds targeting in favor of 
# nonborrowed reserves targeting as the operating procedure for controlling the
# nation's money supply



# OBSERVING IN DETAIL THE EXACT TIMING OF THE INTERVENTIONS
plot(df[index(df) >= "1979-01-01" & index(df) < "1980-10-01",1])
# The FED Experiment reform was implemented in the lat quarter of 1979
plot(df[index(df) >= "1979-01-01" & index(df) < "1980-10-01",3])
# Unemployment started rising simultaneously to the monetary reform
plot(df[index(df) >= "1979-01-01" & index(df) < "1980-10-01",5])
# Inflation shows an increasing trend
# data["/1978"] alternative way to use date

# Source of FED Experiment
# https://www.federalreserve.gov/pubs/feds/2005/200502/200502pap.pdf

# FOR MOST RECENT DATA ANALYSIS AND POTENTIAL INTERVENTION POINT IN RECETN US ECONOMIC HISTORY -> MONEY SUPPLY EXPANSION POST CRISIS
plot(df[index(df) > "2004-04-01",1])
# For the estimations in the project, we will take into consideration the effect of this policy 

# Source: https://www.cnbc.com/2017/11/24/the-fed-launched-qe-nine-years-ago--these-four-charts-show-its-impact.html



