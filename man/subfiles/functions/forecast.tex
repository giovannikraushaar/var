\subsubsection{forecast}

\paragraph{Description}
This function tries to forecast the future levels of given time series at a given time horizon based on coefficients previously estimated.
\paragraph{Usage}
\begin{lstlisting}[language=R]
  forecast( data, beta, p )
\end{lstlisting}
%
\paragraph{Inputs} $ $\\

\begin{tabulary}{\textwidth}{lL}
	\texttt{data} 	& an \texttt{xts} dataset, it does not necessarily need to be the same of the \texttt{VAR()} call, but the columns  \textbf{must} represent the same index. \\
	\texttt{beta} 	& matrix of model's coefficients as retrieved by \texttt{table.coefficients()}. \\
	\texttt{p} 		& number of times to iterate the forecast, \emph{i.e.} time horizon.
\end{tabulary}

\paragraph{Outputs}
a matrix containing the forecasted values.

\paragraph{Method}
Given a model on $K$ socio-economic indexes $I^j$ with $l$ lags for each variable and a dataset $X$ with $n$ observation ordered from the oldest to the most recent one, we define as $P$ the target time horizon and as $p$ the current time horizon. We define the beta matrix -- in which each column is the vector of coefficient of the regression taken with respect of different index -- as
\[
	B = 
	\begin{bmatrix}
		B_{I^1} & \cdots & B_{I^K} 
	\end{bmatrix}_{lK+1\times K}
	\qquad \mathrm{where} \quad  B'_{I^j} =
	\begin{bmatrix}
		\beta_0^{I^j} & \cdots & \beta_{Kl}^{I^j}
	\end{bmatrix}
\]
We can calculate the forecast at the time horizon $P$ by iterating $P+1$ times\footnote{we must iterate one extra time for compliance with the \texttt{delay} function which is exploited by this algorithm  for creating the starting $X_0$ matrix. It makes $X_0$ start from time $t-1$ instead of time $t$. This problem can be bypassed by inserting one empty row with the function \texttt{add\_empty\_line()} at the bottom of the input dataset. An example of this can be found in the \texttt{AFED\_analysis.R} script.} the following calculation:

\begin{gather*}
	X_p =
	\begin{bmatrix}
		\mathbf{1} &  \hat I^1_{p} & \cdots & \hat I^K_{p} & \cdots &  \hat I^1_{p-l+1} & \cdots & \hat I^K_{p-l+1}
	\end{bmatrix}_{n-l\,\times\, lK+1}
	\\
	\hat I^j_p = X_{p-1} B_{I^j} = 
	\begin{bmatrix}
		\hat i^p_{l+p} \\ \vdots \\ \hat i^p_{n-1+p}
	\end{bmatrix}_{n-l\,\times\, 1}
\end{gather*}

\subparagraph{Example} suppose that there are two indexes $M$ and $Y$, a model with 2 time lags and we want to forecast levels 3 periods ahead (with respect to $t-1$). Then
\begin{align*}
		B = &
	\begin{bmatrix}
		B_M & B_Y
	\end{bmatrix}
	&
	X_0 = &
	\begin{bmatrix}
		\mathbf{1} & M_{t-1} & Y_{t-1} & M_{t-2} & Y_{t-2}
	\end{bmatrix}
\end{align*}
Because the \texttt{delay} function is used on 2 lags, the initial $M$ time series becomes
\begin{align*}
	M'_{t-1} = &
	\begin{bmatrix}
		m_2 & \cdots & m_{n-1}
	\end{bmatrix}_{2\times n-2}
	& 
	M'_{t-2} = &
	\begin{bmatrix}
		m_1 & \cdots & m_{n-2}
	\end{bmatrix}_{2\times n-2}
\end{align*}
So that each vector in the $X_0$ matrix has the same number of elements. Therefore the first iteration can be computed.
\begin{align*}
	\hat M_1 = X_0 B_M = &
	\begin{bmatrix}
		\hat m_3 \\ \vdots \\ \hat m_n
	\end{bmatrix}
	& 
	\hat Y_1 = X_0 B_Y = & 
	\begin{bmatrix}
		\hat y_3 \\ \vdots \\ \hat y_n
	\end{bmatrix}
\end{align*}
%
It follows that the first iteration $\hat M_1$ forecasts vector $M_t$ without the first $l=2$ elements, \emph{i.e} it exactly the same of the fitted values of the regression\footnote{this is why we need to iterate $P+1$ times if we want a time horizon of $P$ with respect to the available data.} (if the same dataset is used). With these vectors we can build $X_1$ and then iterate again. 
\[
	X_1 = 
	\begin{bmatrix}
		\mathbf{1} & \hat M_1 & \hat Y_1 & M_{t-1} & Y_{t-1}
	\end{bmatrix}
\]
\begin{align*}
	\hat M_2 = X_1 B_M &\qquad \hat Y_2 = X_1 B_Y & &\Rightarrow & X_2 &=
	\begin{bmatrix}
		\mathbf{1} & \hat M_2 & \hat Y_2 & \hat M_1 & \hat Y_1
	\end{bmatrix} 
\end{align*}
\begin{align*}
	\hat M_3 &= X_2 B_M = 
	\begin{bmatrix}
		\hat m^3_5 & \cdots & \hat m^3_{n+2}
	\end{bmatrix}'_{1\times n-2}
	& 
	\hat Y_3 &= X_2 B_Y =
	\begin{bmatrix}
		\hat y^3_5 & \cdots & \hat y^3_{n+2}
	\end{bmatrix}'_{1\times n-2}
\end{align*}

 
 \noindent\rule[0.5ex]{\linewidth}{1pt}