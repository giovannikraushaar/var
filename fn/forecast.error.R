# giovanni.kraushaar@usi.ch
# 2018-12-12

# forecast.error -------------

forecast.error <- function( data, beta, p, geometry = 'step' ){
  # Input:  data - xts object with same columns used in the VAR() function to which beta refers to
  #                (NO rows must be deleted from it)
  #         beta - beta matrix of the VAR model 
  #                (as retrieved with table.coefficients() function)
  #         p    - number of times to iterate the forcast (ie. ho many periods ahead or forecast horizon)
  #         geometry - geom for plotting, options are 'step' or 'line'
  # Output: a list containing:
  #           * forecast = time series of forcasted leves
  #           * observed = time series of observed levels (matching with forecast)
  #           * error    = errors between matching datapoints in observed - forecast
  #           * plots    = list of plots with obs and forecast (one for each index)
  #           * MFE      = Mean Forecast Error for each variable
  #           * MAD      = Mean Absolute Deviation (from real data, not from mean) 
  #                        for each variable
  
  n <- nrow(data)
  l <- (nrow(beta)-1) / ncol(beta)
  b <- beta
  periods <- p
  name <- colnames(beta)
  
  if((l+p)>n) stop('Not enough data points')
  
  f <- data[1:(n-p+1),] 
  f <- forecast( f, beta = b, p = periods ) # forecasted data
  colnames(f) <- name
  
  d <- data[(l+p):n,] # real data matching with f
  idx <- index(d)
  d <- d %>% coredata() %>% matrix(ncol = ncol(f))
  colnames(d) <- name
  
  e <- d-f # errors after p forecast iterations

  dx <- xts( d, idx) # observed data as xts object
  fx <- xts( f, idx) # forecast as xts object

  pl <- list()  # list of plots
  for (k in 1:ncol(f)) {
    df  <- data_frame( t = idx, observed = d[,k], forecasted = f[,k] )
    df  <- tidyr::gather( df, key = 'type', value = 'value', c('observed','forecasted'))
    sub <- paste( 'forecast made using real data from', p, 'periods before' )
    if( geometry=='step' ){ 
      plo <- ggplot( df, aes(t,value,colour=type) ) + geom_step() + 
        labs( x=NULL, y=NULL, title = name[k], subtitle=sub )
    } else { 
        plo <- ggplot( df, aes(t,value,colour=type) ) + geom_line() +
          labs( x=NULL, y=NULL, title = name[k], subtitle = sub )
    }
    pl[[k]] <- plo
  }
  names(pl) <- name
  for(k in 1:length(pl)){
    pl[[k]]
  }
  
  # https://scm.ncsu.edu/scm-articles/article/measuring-forecast-accuracy-approaches-to-forecasting-a-tutorial
  mfe <- apply( e, 2, mean)
  mad <- apply( e, 2, function(x) mean(abs(x)))
  std.d <- apply(e, 2, sd)
  sem <- apply(e, 2, sd) / apply( e, 2, function(x) sqrt(length(x)))
  
  return(list(
    error = e,
    observed = dx,
    forecast = fx,
    plots = pl,
    MFE = mfe,
    MAD = mad,
    std.dev = std.d,
    std.error = sem
  ))
}
