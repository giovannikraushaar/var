# giovanni.kraushaar@usi.ch
# 2018-12-12

# forecast -------------

forecast <- function( data, beta, p ){
  # Input:   data - same xts object used in the VAR() function
  #                 (some rows can be deleted from that dataset but no rows)
  #          beta - beta matrix of the VAR model 
  #                 (as retrieved with table.coefficients() function)
  #          p    - number of times to iterate the forcast (ie. ho many periods ahead)
  #
  # Output:  a matrix containing the forecasted timeseries for each variable
  #          (only for the last n-l elements because the first l get lost because of
  #          the lags). Each row will be p-1 periods ahead with respect to the starting 
  #          period
  
  K <- ncol(beta)
  l <- (nrow(beta)-1) / K
  n <- nrow(data)
  var.name <- colnames(beta)
  
  if ( n<l ) stop('There are not enough observation to cover all the required lags')
  if ( any(var.name!=names(data)) ) stop('Variables and coefficients are not matching')
  
  d <- data
  d <- delay(d, lags = l)
  X <- coredata(d) %>% matrix(ncol = K*l, nrow = n-l)
  X <- cbind( rep(1, nrow(X)), X )
  out <- NULL 
  
  for (j in 1:p){
    Xnew <- matrix( NA, ncol = ncol(X), nrow = nrow(X) )
    Xnew[,1] <- rep(1, nrow(X))
    
    for( k in 1:K ){
      Xnew[,k+1] <- X %*% beta[,k]
    } ; rm(k)
    
    Xnew[,(K+2):(l*K+1)] <- X[,2:(K*l-K+1)] 
    # Shifting the vectors right K positions and removing last K.
    X <- Xnew
    out_tmp <- X[,2:(K+1)]
    names(out_tmp) <- var.name
    out[[j]] <- out_tmp
  }

  return(out[[p]])
}

# TODO further development: this function could easily output a 3d object in which
# in the third dimension there are the past X matrixes. This would reduce the 
# computation required by a lot if mutiple periods prediction are needed (as 
# in forecastStandardErrors()).

# it is not difficult... just it requires time (which we do not have)