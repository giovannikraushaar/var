# giovanni.kraushaar@usi.ch
# 2018-12-14

# variance.decomposition -------------

variance.decomposition <- function( u ){
  # Input:   u - forecast error matrix
  #          collapse - 
  # Output:  matrix of forecasted error decomposition
  name <- colnames(u)
  
  A     <- recover.structural(u)
  A.inv <- solve(A)
  
  V.dec <- apply( A.inv, 1, function(x) (x*x / sum(x*x)) * 100 ) # * 100 in pp as Stock Watson Table 1.B
  V.dec <- lapply( as.list(1:ncol(A)), function(x) V.dec[,x] )
  names(V.dec) <- name
  
  return(V.dec)
}
