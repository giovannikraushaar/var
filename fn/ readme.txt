Giovanni Kraushaar
readme

In this folder (fn) we put all the functions that we are going to use in the project.
In order to have a clear and consistent code, please stick to the following rules:

(a) Use one .R script per function.
(b) Give the .R script the same name of your function (respect case sensitivity)
(c) If you need a function that has been created earlier, import it with source().
(d) There is a template, called template.R that you can use as a reference. 
(e) Use comments inside the function (on the top) only for core stuff, 
    so that it can give a quick reference by calling the function. 
    For more detailed comments about the function use comments after the function.
(f) Use comments after a line of code to explain the rationale of cite the source of
    the code as much as you wish.